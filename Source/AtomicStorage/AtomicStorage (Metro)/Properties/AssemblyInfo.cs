﻿///-----------------------------------------------------------------------
/// Project: AtomicMVVM https://bitbucket.org/rmaclean/atomicmvvm
/// License: MS-PL http://www.opensource.org/licenses/MS-PL
/// Notes:
///-----------------------------------------------------------------------

using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("AtomicStorage")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Robert MacLean")]
[assembly: AssemblyProduct("AtomicMVVM")]
[assembly: AssemblyCopyright("Copyright © Robert MacLean 2012")]
[assembly: AssemblyCulture("en")]
[assembly: System.CLSCompliant(false)]
[assembly: System.Resources.NeutralResourcesLanguage("en")]
[assembly: ComVisible(false)]
[assembly: AssemblyVersion("1.1.0.0")]
[assembly: AssemblyFileVersion("1.1.0.0")]
